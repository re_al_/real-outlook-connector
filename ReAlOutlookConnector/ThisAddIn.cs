﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EvernoteSDK;
using WinOauth;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

namespace ReAlOutlookConnector
{
    public partial class ThisAddIn
    {
        Outlook.Inspectors inspectors;
        public static Outlook.MailItem miCorreo;
        public static OAuthTrello _oauth = new OAuthTrello();
        public static String strTrelloApiKey;
        public static String strTrelloToken;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            try
            {
                //Iniciamos Evernote
                ENSession.SetSharedSessionConsumerKey("77323", "e9b13f228965ef7c"); 
                if (ENSession.SharedSession.IsAuthenticated == false)
                {
                    ENSession.SharedSession.AuthenticateToEvernote();
                }

                //Iniciamos Trello
                /*
                String requestToken = _oauth.getRequestToken();
                _oauth.authorizeToken();
                String accessToken = _oauth.getAccessToken();
                ThisAddIn.strTrelloApiKey = _oauth.ConsumerKey;
                ThisAddIn.strTrelloToken = accessToken;
                */
                

                //Instanciamos los eventos
                inspectors = this.Application.Inspectors;
                inspectors.NewInspector += Inspectors_NewInspector;            
            }
            catch (Exception exp)
            {
                cErrores cErr = new cErrores();
                cErr.reportarError(exp);
                MessageBox.Show("Ha ocurrido un error al iniciar. Inténtelo de nuevo o contactese con el Administrador\r\n\r\n" + exp.Message + "\r\n" + exp.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        void Inspectors_NewInspector(Outlook.Inspector Inspector)
        {
            try
            {
                //  This function (apparently) gets kicked off whenever a user opens a new or existing item
                //  in Outlook (Calendar appointment, Email, etc).  
                //  We can intercept it, modify it's properties, before letting our Ribbon know about it's existance.
                //
                miCorreo = null;

                object item = Inspector.CurrentItem;

                //MessageBox.Show(item.ToString());

                if (item == null)
                    return;

                if (!(item is Outlook.MailItem))
                    return;
                miCorreo = Inspector.CurrentItem as Outlook.MailItem;
            }
            catch (Exception exp)
            {
                cErrores cErr = new cErrores();
                cErr.reportarError(exp);
                MessageBox.Show("Ha ocurrido un error al iniciar. Inténtelo de nuevo o contactese con el Administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            inspectors.NewInspector -= new Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);

            inspectors = null;
            miCorreo = null;
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
