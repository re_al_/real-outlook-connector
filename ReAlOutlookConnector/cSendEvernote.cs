﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EvernoteSDK;

namespace ReAlOutlookConnector
{
    public class cSendEvernote
    {

        public static void send()
        {
            try
            {
                if (ENSession.SharedSession.IsAuthenticated)
                {
                    Microsoft.Office.Interop.Outlook.Application oApp = new Microsoft.Office.Interop.Outlook.Application();
                    Microsoft.Office.Interop.Outlook.Explorer oExp = oApp.ActiveExplorer();
                    Microsoft.Office.Interop.Outlook.Selection oSel = oExp.Selection;  // You need a selection object for getting the selection.
                    Object oItem; // You don't know the type yet.

                    //MessageBox.Show("Seleccion: " + oSel.Count + " en " + oExp.Caption);
                    String strMessageClass = "";

                    for (int i = 0; i < oSel.Count; i++)
                    {
                        oItem = oSel[i + 1];
                        if (oItem is Microsoft.Office.Interop.Outlook.MailItem)
                        {
                            Microsoft.Office.Interop.Outlook.MailItem oMail = (Microsoft.Office.Interop.Outlook.MailItem)oItem;

                            if (MessageBox.Show("¿Quieres enviar éste correo a Evernote?", "Evernote", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.Yes)
                            {
                                try
                                {
                                    //  Our Ribbon control contains a TextBox called "tbSubject"
                                    ENNote myFancyNote = new ENNote();
                                    if (string.IsNullOrEmpty(oMail.Subject))
                                        myFancyNote.Title = oMail.SenderEmailAddress.ToString();
                                    else
                                        myFancyNote.Title = oMail.Subject.ToString();

                                    String strAdjuntos = "\r\n\r\nAdjuntos:\r\n--------------------------------------------------------------";
                                    String strCabecera = "De: " + oMail.SenderName + "<" + oMail.SenderEmailAddress + ">\r\nFecha: " + oMail.ReceivedTime + "\r\n--------------------------------------------------------------\r\n";
                                    foreach (Microsoft.Office.Interop.Outlook.Attachment adjunto in oMail.Attachments)
                                    {
                                        strAdjuntos = strAdjuntos + "\r\n" + adjunto.FileName;
                                    }

                                    if (oMail.Attachments.Count == 0)
                                        myFancyNote.Content = ENNoteContent.NoteContentWithString(strCabecera + oMail.Body);
                                    else
                                        myFancyNote.Content = ENNoteContent.NoteContentWithString(strCabecera + oMail.Body + strAdjuntos);

                                    ENNoteRef myFancyNoteRef = ENSession.SharedSession.UploadNote(myFancyNote, null);
                                    MessageBox.Show("Se ha agregado el correo seleccionado a Evernote");
                                }
                                catch (Exception exp)
                                {
                                    MessageBox.Show(exp.Message + "\r\n\r\n" + exp.StackTrace);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                cErrores cErr = new cErrores();
                cErr.reportarError(exp);
                MessageBox.Show("Ha ocurrido un error. Inténtelo de nuevo o contactese con el Administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string getAttachmentPath(Microsoft.Office.Interop.Outlook.Attachment attachment)
        {
            var path = System.IO.Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), attachment.FileName);
            attachment.SaveAsFile(path);
            return path;
        }

        private string GetMimeTypeFromFileExtension(string extension)
        {
            string mimeType = "";
            extension = extension.ToLower();
            switch (extension)
            {
                case ".png":
                    mimeType = "image/png";
                    break;
                case ".jpg":
                    mimeType = "image/jpg";
                    break;
                case ".bmp":
                    mimeType = "image/bmp";
                    break;
                case ".gif":
                    mimeType = "image/gif";
                    break;
                case ".doc":
                    mimeType = "document/doc";
                    break;
                case ".docx":
                    mimeType = "document/docx";
                    break;
                case ".xls":
                    mimeType = "document/xls";
                    break;
                case ".xlsx":
                    mimeType = "document/xlsx";
                    break;
                case ".pdf":
                    mimeType = "document/pdf";
                    break;
                case ".rtf":
                    mimeType = "docuemnt/rtf";
                    break;
                case ".zip":
                    mimeType = "archive/zip";
                    break;
                case ".rar":
                    mimeType = "archive/rar";
                    break;
                default:
                    mimeType = "binary/other";
                    break;
            }
            return mimeType;
        }
    }
}
