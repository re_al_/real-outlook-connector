﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ReAl_BitBucket;
using ReAl_BitBucket.Controllers;
using ReAl_BitBucket.Models;

namespace ReAlOutlookConnector
{
    public class cErrores
    {
        [DllImport("kernel32.dll")]
        internal static extern uint GetTickCount();
        private const int altoConDetalles = 331;
        private const int altoSinDetalles = 182;
        private ListView listAuxiliar = new ListView();
        private string strTrazaError;
        private readonly string _errorDirectory = string.Format(@"{0}\Errors", Environment.CurrentDirectory);
        private string _strErrorLog = "";

        private Exception _myExp;        
        private string _myTitulo;



        public void reportarError(Exception exp)
        {
            _myExp = exp;

            try
            {
                _myExp.Data.Add("Nombre Equipo", Environment.MachineName);
                _myExp.Data.Add("Nombre Dominio", Environment.UserDomainName);
                _myExp.Data.Add("Nombre Usuario SO", Environment.UserName);

                string strHostName = Dns.GetHostName();
                IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
                int i = 1;
                foreach (IPAddress ipaddress in iphostentry.AddressList)
                {
                    _myExp.Data.Add("Direccion IP " + i, ipaddress.ToString());
                    i++;
                }
                crearEntornoExcepcion(_myExp);
                Thread thread = new Thread(new ThreadStart(crearExcepcionIssue));
                thread.Start();                
            }
            catch (Exception)
            {

            }
        }

        private void crearExcepcionIssue()
        {
            try
            {
                Client cli = new Client(cParametrosIssues.strBitBucketUser, cParametrosIssues.strBitBucketPassword);

                CreateIssueModel miIssue = new CreateIssueModel();
                miIssue.Title = _myExp.Message;
                miIssue.Content = crearIssue(_myExp);
                miIssue.Kind = "bug";
                miIssue.Priority = "critical";
                RepositoryController repo = new RepositoryController(cli, new UserController(cli, cParametrosIssues.strBitBucketUser), cParametrosIssues.strBitBucketRepository);
                repo.Issues.Create(miIssue);
            }
            catch (Exception eIgnored)
            {

            }
        }

        private static String crearIssue(Exception exp)
        {
            StringBuilder strIssue = new StringBuilder();

            strIssue.AppendLine("Nombre Aplicacion: " + Application.ProductName + Environment.NewLine);
            strIssue.AppendLine("Nombre Equipo: " + Environment.MachineName + Environment.NewLine);
            strIssue.AppendLine("Nombre Dominio: " + Environment.UserDomainName + Environment.NewLine);
            strIssue.AppendLine("Nombre Usuario SO: " + Environment.UserName + Environment.NewLine);            

            string strHostName = Dns.GetHostName();
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
            int i = 1;
            foreach (IPAddress ipaddress in iphostentry.AddressList)
            {
                strIssue.AppendLine("Direccion IP " + i + ": " + ipaddress.ToString() + Environment.NewLine);
                i++;
            }

            string strBuildTime = new DateTime(2000, 1, 1).AddDays(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build).ToShortDateString();
            TimeSpan timeSpanProcTime = Process.GetCurrentProcess().TotalProcessorTime;
            DriveInfo driveInfo = new DriveInfo(Directory.GetDirectoryRoot(Application.ExecutablePath));

            strIssue.AppendLine("Fecha y Hora del Error: " + DateTime.Now.ToString() + Environment.NewLine);
            strIssue.AppendLine("Fecha y Hora de Ejecucion: " + Process.GetCurrentProcess().StartTime.ToString() + Environment.NewLine);
            strIssue.AppendLine("Fecha del Build: " + strBuildTime + Environment.NewLine);
            strIssue.AppendLine("Sistema Operativo: " + Environment.OSVersion.VersionString + Environment.NewLine);
            strIssue.AppendLine("Plataforma: " + Environment.OSVersion.Platform.ToString() + Environment.NewLine);
            strIssue.AppendLine("Languaje: " + Application.CurrentInputLanguage.LayoutName + Environment.NewLine);
            strIssue.AppendLine("Tiempo de Ejecucion del Sistema: " + string.Format("{0} Days {1} Hours {2} Mins {3} Secs", Math.Round((decimal)GetTickCount() / 86400000), Math.Round((decimal)GetTickCount() / 3600000 % 24), Math.Round((decimal)GetTickCount() / 120000 % 60), Math.Round((decimal)GetTickCount() / 1000 % 60)) + Environment.NewLine);
            strIssue.AppendLine("Tiempo de Ejecucion de la Aplicacion: " + string.Format("{0} hours {1} mins {2} secs", timeSpanProcTime.TotalHours.ToString("0"), timeSpanProcTime.TotalMinutes.ToString("0"), timeSpanProcTime.TotalSeconds.ToString("0")) + Environment.NewLine);
            strIssue.AppendLine("PID: " + Process.GetCurrentProcess().Id.ToString() + Environment.NewLine);
            strIssue.AppendLine("Thread Count: " + Process.GetCurrentProcess().Threads.Count.ToString() + Environment.NewLine);
            strIssue.AppendLine("Thread Id: " + System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() + Environment.NewLine);
            strIssue.AppendLine("Ejecutable: " + Application.ExecutablePath + Environment.NewLine);
            strIssue.AppendLine("Nombre del Proceso: " + Process.GetCurrentProcess().ProcessName + Environment.NewLine);
            strIssue.AppendLine("Version: " + Application.ProductVersion + Environment.NewLine);
            strIssue.AppendLine("CLR Version: " + Environment.Version.ToString() + Environment.NewLine);
            strIssue.AppendLine("" + Environment.NewLine);

            Exception ex = exp;
            for (int j = 0; ex != null; ex = ex.InnerException, j++)
            {
                strIssue.AppendLine("Tipo #" + j + ": " + ex.GetType().ToString());
                if (!string.IsNullOrEmpty(ex.Message))
                    strIssue.AppendLine("Mensaje #" + j + ": " + ex.Message + Environment.NewLine);
                if (!string.IsNullOrEmpty(ex.Source))
                    strIssue.AppendLine("Origen #" + j + ": " + ex.Source + Environment.NewLine);
                if (!string.IsNullOrEmpty(ex.HelpLink))
                    strIssue.AppendLine("Enlace de ayuda #" + j + ": " + ex.HelpLink + Environment.NewLine);
                if (ex.TargetSite != null)
                    strIssue.AppendLine("Target Site #" + j + ": " + ex.TargetSite.ToString() + Environment.NewLine);
                if (ex.Data != null)
                {
                    foreach (DictionaryEntry de in ex.Data)
                    {
                        strIssue.AppendLine(de.Key.ToString() + ": " + de.Value.ToString() + Environment.NewLine);
                    }
                }
                strIssue.AppendLine("" + Environment.NewLine);
            }

            strIssue.AppendLine("" + Environment.NewLine);
            strIssue.AppendLine(exp.StackTrace);

            return strIssue.ToString();
        }

        private void crearEntornoExcepcion(Exception e)
        {
            string strBuildTime = new DateTime(2000, 1, 1).AddDays(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build).ToShortDateString();

            // Gets program uptime
            TimeSpan timeSpanProcTime = Process.GetCurrentProcess().TotalProcessorTime;

            // Used to get disk space
            DriveInfo driveInfo = new DriveInfo(Directory.GetDirectoryRoot(Application.ExecutablePath));

            listAuxiliar.Items.Add(new ListViewItem(new[] { "Current Date/Time", DateTime.Now.ToString() }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Exec. Date/Time", Process.GetCurrentProcess().StartTime.ToString() }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Build Date", strBuildTime }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "OS", Environment.OSVersion.VersionString }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Plataform", Environment.OSVersion.Platform.ToString() }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Language", Application.CurrentInputLanguage.LayoutName }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "System Uptime", string.Format("{0} Days {1} Hours {2} Mins {3} Secs", Math.Round((decimal)GetTickCount() / 86400000), Math.Round((decimal)GetTickCount() / 3600000 % 24), Math.Round((decimal)GetTickCount() / 120000 % 60), Math.Round((decimal)GetTickCount() / 1000 % 60)) }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Program Uptime", string.Format("{0} hours {1} mins {2} secs", timeSpanProcTime.TotalHours.ToString("0"), timeSpanProcTime.TotalMinutes.ToString("0"), timeSpanProcTime.TotalSeconds.ToString("0")) }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "PID", Process.GetCurrentProcess().Id.ToString() }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Thread Count", Process.GetCurrentProcess().Threads.Count.ToString() }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Thread Id", System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Executable", Application.ExecutablePath }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Process Name", Process.GetCurrentProcess().ProcessName }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "Version", Application.ProductVersion }));
            listAuxiliar.Items.Add(new ListViewItem(new[] { "CLR Version", Environment.Version.ToString() }));

            Exception ex = e;
            for (int i = 0; ex != null; ex = ex.InnerException, i++)
            {
                listAuxiliar.Items.Add(new ListViewItem(new[] { "Type #" + i, ex.GetType().ToString() }));
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    listAuxiliar.Items.Add(new ListViewItem(new[] { "Message #" + i, ex.Message }));
                }
                if (!string.IsNullOrEmpty(ex.Source))
                {
                    listAuxiliar.Items.Add(new ListViewItem(new[] { "Source #" + i, ex.Source }));
                }
                if (!string.IsNullOrEmpty(ex.HelpLink))
                {
                    listAuxiliar.Items.Add(new ListViewItem(new[] { "Help Link #" + i, ex.HelpLink }));
                }
                if (ex.TargetSite != null)
                {
                    //listException.Items.Add(new ListViewItem(new[] { "Target Site #" + i, ex.TargetSite.ToString() }));
                    listAuxiliar.Items.Add(new ListViewItem(new[] { "Target Site #" + i, ex.TargetSite.ToString() }));
                }
                if (ex.Data != null)
                {
                    foreach (DictionaryEntry de in ex.Data)
                    {
                        listAuxiliar.Items.Add(new ListViewItem(new[] { de.Key.ToString(), de.Value.ToString() }));
                    }
                }
            }

            strTrazaError = e.StackTrace;

            _strErrorLog = string.Format("{0}\\{1:yyyy}_{1:MM}_{1:dd}_{1:HH}{1:mm}{1:ss}.log", _errorDirectory, DateTime.Now);
            CreateErrorLog();
        }

        /// <summary>
        /// Creates error log file
        /// </summary>
        /// <returns>True on success</returns>
        private void CreateErrorLog()
        {
            StreamWriter streamErrorLog;

            // Create directory if it doesnt exist
            if (!Directory.Exists(_errorDirectory))
                Directory.CreateDirectory(_errorDirectory);

            try
            {
                streamErrorLog = File.CreateText(_strErrorLog);

                int i;

                streamErrorLog.WriteLine();

                for (i = 0; i < listAuxiliar.Items.Count; i++)
                {
                    string strDesc = listAuxiliar.Items[i].SubItems[0].Text;
                    string strValue = listAuxiliar.Items[i].SubItems[1].Text;

                    streamErrorLog.WriteLine(string.Format("{0}: {1}", strDesc, strValue));
                }

                streamErrorLog.WriteLine("Stack Trace:");
                streamErrorLog.WriteLine(strTrazaError);
                streamErrorLog.Close();
            }
            catch
            {
                return;
            }
            return;
        }
    }
}
