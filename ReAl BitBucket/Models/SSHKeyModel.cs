namespace ReAl_BitBucket.Models
{
    public class SSHKeyModel
    {
        public string Pk { get; set; }
        public string Key { get; set; }
    }
}

