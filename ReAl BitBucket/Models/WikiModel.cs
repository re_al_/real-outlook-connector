﻿namespace ReAl_BitBucket.Models
{
    public class WikiModel
    {
        public string Data { get; set; }
        public string Rev { get; set; }
        public string Markup { get; set; }
    }
}
