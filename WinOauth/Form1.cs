﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinOauth
{
    public partial class Form1 : Form
    {

        private OAuthTrello _oauth = new OAuthTrello();

        public Form1()
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnProbar_Click(object sender, EventArgs e)
        {
            try
            {
                String requestToken = _oauth.getRequestToken();
                txtOutput.Text += "\n" + "Received request token: " + requestToken;

                _oauth.authorizeToken();
                txtOutput.Text += "\n" + "Token was authorized: " + _oauth.Token + " with verifier: " + _oauth.Verifier;
                String accessToken = _oauth.getAccessToken();
                txtOutput.Text += "\n" + "Access token was received: " + _oauth.Token;
            }
            catch (Exception exp)
            {
                txtOutput.Text += "\nException: " + exp.Message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
