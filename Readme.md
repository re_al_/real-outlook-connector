ReAl Outlook Connector
=========

ReAl Outlook Connector es una add-in para Ms Outlook 2010-2013 con las siguientes caracteristicas.

  - Enviar uno o varios correos a Evernote
  
Para obtener una copia del instalador de la Ultima versión, visitar el área de [descargas] [1]:


Version
----

  - 0.1


Trello Board
----

  - [ReAl Outlook Connector Board]
  
Reporistorio BitBucket
----

  - [ReAl Outlook Connector Source]
  

Equipo de Desarrollo
-----------

ReAl Outlook Connector fue desarrollado por:

Ing. Reynaldo Alonzo Vera Arias 

  - [Pagina Web]
  - [Correo Electrónico]
  - [Twiter]



[1]:https://bitbucket.org/re_al_/real-outlook-connector/downloads
[Pagina Web]:http://real7.somee.com/
[Correo Electrónico]:mailto:7.re.al.7@gmail.com?Subject=NeuroDiagnostico
[Twiter]:https://twitter.com/re_al_
[ReAl Outlook Connector Board]:https://trello.com/b/yolbteRm/real-outlook-connector
[ReAl Outlook Connector Source]:https://bitbucket.org/re_al_/real-outlook-connector

